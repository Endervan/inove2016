<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  205px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class='container '>
    <div class="row">

      <div class="col-xs-8 padding0">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">TRABALHE CONOSCO</a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- TITULO BANNER E DESCRICAO -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 titulo_contatos padding0 font-futura  top35 text-right">
          <h5>TRABALHE CONOSCO</h5>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO BANNER  E DESCRICAO  -->
        <!-- ======================================================================= -->


        <div class="col-xs-12  top60 padding0 ">

          <!-- ======================================================================= -->
          <!-- CONTATOS e UNIDADES-->
          <!-- ======================================================================= -->
          <?php require_once('./includes/contatos_unidades.php') ?>
          <!-- ======================================================================= -->
          <!-- CONTATOS e UNIDADES-->
          <!-- ======================================================================= -->


          <!--  ==============================================================  -->
          <!-- FORMULARIO-->
          <!--  ==============================================================  -->
          <form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
            <div class="fundo_formulario">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top15"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top15"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>


              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top15"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                    <span class="fa fa-book form-control-feedback top15"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                    <span class="fa fa-lock form-control-feedback"></span>
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                    <span class="fa fa-briefcase form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="top20">

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                    <span class="fa fa-globe form-control-feedback top15"></span>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                    <span class="fa fa-globe form-control-feedback top15"></span>
                  </div>
                </div>

              </div>


              <div class="clearfix"></div>

              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                    <span class="fa fa-file form-control-feedback top15"></span>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>


              <div class="top20">
                <div class="col-xs-12">
                  <div class="form-group input100 has-feedback">
                    <textarea name="mensagem" cols="30" rows="9" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback top15"></span>
                  </div>
                </div>
              </div>


              <div class="col-xs-6 top30">
                <img src="<?php echo Util::caminho_projeto(); ?>/imgs/saiba_como_chegar_contatos.jpg" alt="" />
              </div>
              <div class="col-xs-6 text-right">
                <div class="top15 bottom25">
                  <button type="submit" class="btn btn-formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>
            </div>


            <!--  ==============================================================  -->
            <!-- FORMULARIO-->
            <!--  ==============================================================  -->
          </form>

          <!--  ==============================================================  -->
          <!-- FORMULARIO-->
          <!--  ==============================================================  -->
        </div>

      </div>
      <!--  ==============================================================  -->
      <!-- bg lateral ->
      <!-  ==============================================================  -->
      <div class="col-xs-4  top10">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/bg_contatos_fale.png" alt="" />
      </div>
      <!--  ==============================================================  -->
      <!-- bg lateral ->
      <!- ==============================================================  -->
    </div>
  </div>





  <div class='container '>
    <div class="row">

      <div class="col-xs-12">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="508" frameborder="0" style="border:0" allowfullscreen></iframe>

        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Email: ".$_POST[email]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Cargo: ".$_POST[cargo]." <br />
  Área: ".$_POST[area]." <br />
  Cidade: ".$_POST[cidade]." <br />
  Estado: ".$_POST[estado]." <br />
  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";

  Util::envia_email($config_email[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config_email[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}

?>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'insira email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
