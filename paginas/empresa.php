<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  205px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class='container '>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">A EMPRESA</a>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row">
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->
      <div class="col-xs-5 titulo_cat font-futura top35 text-right">
        <h5>CONHEÇA MAIS</h5>
        <h5><span>NOSSA EMPRESA</span></h5>
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>

        <div class="top20">
          <p><?php Util::imprime($row[descricao],1000); ?></p>
        </div>

      </div>
      <!-- ======================================================================= -->
      <!-- TITULO BANNER E DESCRICAO -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- SLIDER EMPRESA   -->
      <!-- ======================================================================= -->
      <div class="col-xs-7">



        <?php require_once('./includes/slider_empresa.php') ?>
        <div class="borda_empresa"></div>
      </div>
      <!-- ======================================================================= -->
      <!-- SLIDER EMPRESA   -->
      <!-- ======================================================================= -->
    </div>
  </div>




  <div class="container-fluid bg_empresa_valores">
    <div class="row">
      <div class="container">
        <div class="row">
          <!-- ======================================================================= -->
          <!-- valores empresa   -->
          <!-- ======================================================================= -->
          <div class="col-xs-offset-3 col-xs-9  top50">

            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
            <div class="col-xs-12 empresa_valores pt10 top20">
              <h3><?php Util::imprime($row[titulo]); ?></h3>
              <div class="scrol_empresa">	<p><?php Util::imprime($row[descricao],1000); ?></p></div>
            </div>

            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
            <div class="col-xs-12 empresa_valores pt10 top20">
              <h3><?php Util::imprime($row[titulo]); ?></h3>
              <div class="scrol_empresa">	<p><?php Util::imprime($row[descricao],1000); ?></p></div>
            </div>


            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
            <div class="col-xs-12 empresa_valores pt10 top20">
              <h3><?php Util::imprime($row[titulo]); ?></h3>
              <div class="scrol_empresa">	<p><?php Util::imprime($row[descricao],1000); ?></p></div>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- valores empresa   -->
          <!-- ======================================================================= -->



          <!-- ======================================================================= -->
          <!-- SLIDER CLIENTES   -->
          <!-- ======================================================================= -->
          <div class="col-xs-8 top80 padding0">

            <!-- ======================================================================= -->
            <!--SLIDER CLIENTES -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/slider_clientes.php'); ?>
            <!-- ======================================================================= -->
            <!--SLIDER CLIENTES -->
            <!-- ======================================================================= -->
          </div>

          <div class="col-xs-4 text-center top105">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/nosso_clientes_home.png" alt="" />
          </div>
          <!-- ======================================================================= -->
          <!-- SLIDER CLIENTES   -->
          <!-- ======================================================================= -->
        </div>
      </div>
    </div>
  </div>


  <div class="clearfix">  </div>
  <!-- ======================================================================= -->
  <!-- SAIBA COMO CHEGAR   -->
  <!-- ======================================================================= -->
  <div class='container top120'>
    <div class="row">
      <div class="col-xs-3 font-futura top105">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba_como_chegar.png" alt="" />
        <!-- ======================================================================= -->
        <!-- UNIDADES   -->
        <!-- ======================================================================= -->
        <div class="top15 unidades tabs_unidades">



            <!-- Nav tabs -->
            <ul class="nav nav-pill" role="tablist">
              <?php
              $result = $obj_site->select("tb_unidades","order by rand() limit 5");
              if (mysql_num_rows($result) > 0) {
                $i = 0;
                while($row = mysql_fetch_array($result)){
                  $unidades[] = $row;
                ?>
                  <li role="presentation" class="<?php if($i == 0){ echo 'active'; } ?>"><a href="#tab_<?php Util::imprime($row[idunidade]); ?>" aria-controls="home" role="tab" data-toggle="tab" class=""><?php Util::imprime($row[titulo]); ?></a></li>
                <?php
                $i++;
                }
              }
              ?>
            </ul>


        </div>
        <!-- ======================================================================= -->
        <!-- UNIDADES   -->
        <!-- ======================================================================= -->
      </div>

      <div class="col-xs-9 top150">

        <!-- Tab panes -->
        <div class="tab-content ">
          <?php
          if (count($unidades) > 0) {
            $i = 0;
            foreach ($unidades as $key => $row) {
            ?>
              <div role="tabpanel" class="tab-pane <?php if($i == 0){ echo 'active'; } ?>" id="tab_<?php Util::imprime($row[idunidade]); ?>">
                <iframe src="<?php Util::imprime($row[src_place]); ?>" width="100%" height="447" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
            <?php
            $i++;
            }
          }
          ?>

        </div>


      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SAIBA COMO CHEGAR   -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
