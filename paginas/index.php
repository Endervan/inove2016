<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->




  <div class="container">
    <div class="row subir_produtos_destaques">
      <!-- ======================================================================= -->
      <!-- conheca escritorio    -->
      <!-- ======================================================================= -->
      <div class="col-xs-4 bottom40 lato-black">
        <a href="<?php echo Util::caminho_projeto() ?>/produtos" class=" btn_linha_escritorio">
          CONHECER TODA LINHA DE UNIFORMES
        </a>
      </div>
      <!-- ======================================================================= -->
      <!-- conheca escritorio    -->
      <!-- ======================================================================= -->
      <div class="clearfix">  </div>

      <!-- ======================================================================= -->
      <!--PRODUTOS EM DESTAQUES    falta fazer a filtragem-->
      <!-- ======================================================================= -->
      <div class="col-xs-8 lato-black">
        <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_produtos_destaques btn-lg active" role="button">PRODUTOS EM DESTAQUE</a>
         <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_produtos_destaques btn-lg" role="button">PRODUTOS EM PROMOÇÕES </a>
         <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_produtos_destaques btn-lg" role="button">PRODUTOS MAIS VENDIDOS</a>

      </div>
      <!-- ======================================================================= -->
      <!-- PRODUTOS EM DESTAQUES    -->
      <!-- ======================================================================= -->
    </div>
  </div>



  <!-- ======================================================================= -->
  <!--PRODUTOS home    -->
  <!-- ======================================================================= -->
  <div class="container top20">
    <div class="row">
      <?php
			$result = $obj_site->select("tb_produtos","ORDER BY RAND() LIMIT 8");
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_array($result))
				{
					$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
					$row_categoria = mysql_fetch_array($result_categoria);
					?>

          <div class="col-xs-3 produtos_home top30">
            <h3 class="text-right"><?php Util::imprime($row_categoria[titulo]); ?></h3>
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",263, 333, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>

            <div class="top15">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </div>

            <div class="cod_numerao">
              <p>Cód.<?php Util::imprime($row[codigo_produto],100); ?></p>
            </div>

            <div class="col-xs-8 padding0 top15">
              <a class="btn btn_saiba_mais_produtos col-xs-12" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>"  title="SAIBA MAIS">SAIBA MAIS</a>
            </div>

            <div class="col-xs-4">
              <a class="btn btn_saiba_mais_produtos col-xs-12" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" data-toggle="tooltip"  title="Adicionar ao orçamento">
                <img src="<?php echo Util::caminho_projeto(); ?>/imgs/carrinho_produtos.png" alt="">
              </a>
            </div>

          </div>

          <?php
          if ($i == 3) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>


      <div class="clearfix"></div>
      <div class="col-xs-offset-8 col-xs-4 div_personalizada top15">
        <a class="btn btn_ver_todos col-xs-12" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>"  title="VER TODOS OS PRODUTOS">VER TODOS OS PRODUTOS</a>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- PRODUTOS home    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--CONHECA MAIS   -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_conheca_home ">
    <div class="row">
      <div class="container top220">
        <div class="row">



          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

          <div class="col-xs-8 col-xs-offset-4 top30">
            <div class="conheca_mais_home bottom40">
              <p><?php Util::imprime($row[descricao],1000); ?></p>
            </div>

            <div class="col-xs-3 padding0 top15">
              <a class="btn btn_conheca_mais col-xs-12" href="<?php echo Util::caminho_projeto() ?>/empresa"  title="SAIBA MAIS">SAIBA MAIS</a>
            </div>

            <div class="col-xs-5 top15">
              <a class="btn btn_conheca_mais col-xs-12" href="<?php echo Util::caminho_projeto() ?>/fale-conosco"  title="ENTRE EM CONTATO">ENTRE EM CONTATO</a>
            </div>

            <div class="col-xs-4 padding0">
              <a class="btn btn_conheca_mais col-xs-12" href="<?php echo Util::caminho_projeto() ?>/fale-conosco"  title="SAIBA COMO CHEGAR">
                SAIBA COMO CHEGAR
              </a>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CONHECA MAIS   -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--NOSSA DICAS -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-4 text-center top20">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/nossas_dicas_seta.png" alt="" />
      </div>


      <div class="col-xs-8 text-right subir_dicas padding0">
        <div class="col-xs-3  col-xs-offset-9 top15">
          <a class="btn btn_nossas_dicas col-xs-12" href="<?php echo Util::caminho_projeto() ?>/dicas/"  title="VER TODAS AS DICAS">
            VER TODAS AS DICAS <i class="fa fa-angle-right left10"></i>
          </a>
        </div>

        <!-- ======================================================================= -->
        <!--SLIDER DICAS -->
        <!-- ======================================================================= -->
          <?php require_once('./includes/slider_dicas.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER DICAS -->
          <!-- ======================================================================= -->
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- NOSSA DICAS -->
  <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--NOSSOS CLIENTES -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">



        <div class="col-xs-8 padding0">

          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
            <?php require_once('./includes/slider_clientes.php'); ?>
            <!-- ======================================================================= -->
            <!--SLIDER CLIENTES -->
            <!-- ======================================================================= -->
        </div>

        <div class="col-xs-4 text-center top20">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/nosso_clientes_home.png" alt="" />
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS CLIENTES -->
    <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>

<?php require_once('./includes/js_css.php') ?>
