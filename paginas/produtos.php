<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  205px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- Breadcrumbs    -->
  <!-- ======================================================================= -->
  <div class="clearfix"></div>
  <div class='container'>
    <div class="row">
      <div class="col-xs-12">
        <div class="breadcrumb top10">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">PRODUTOS</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- Breadcrumbs    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- TITULO BANNER E DESCRICAO -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-5 titulo_cat padding0 font-futura top10 text-right">
      <h5>CONFIRA TODOS OS</h5>
      <h5><span>NOSSOS PRODUTOS</span></h5>
    </div>

  </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO BANNER    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- PRODUTOS  GERAL -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-4 div_personalizada">
      <a class="btn btn_produtos col-xs-12" href="<?php echo Util::caminho_projeto() ?>/produtos-categoria">
        VER TODOS OS NOSSOS PRODUTOS
      </a>
    </div>

    <div class="clearfix">  </div>

        <!-- ======================================================================= -->
        <!-- TODOS OS PRODUTOS   -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/menu_produtos.php'); ?>
        <!-- ======================================================================= -->
        <!-- TODOS OS PRODUTOS   -->
        <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- TODOS OS PRODUTOS   -->
    <!-- ======================================================================= -->
    <div class="col-xs-4 div_personalizada top25">
      <div class="fundo_produtos_todos pb15 text-center">
        <a href="<?php echo Util::caminho_projeto() ?>/produtos-categoria">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_produto07.png" alt="" />
          <div class="top20">
            <h2>VER TODOS OS NOSSOS PRODUTOS</h2>
          </div>
        </a>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- TODOS OS PRODUTOS   -->
    <!-- ======================================================================= -->


  </div>
</div>
<!-- ======================================================================= -->
<!-- PRODUTOS  GERAL    -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
