<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",5) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  205px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class='container '>
    <div class="row">

      <div class="col-xs-7 padding0">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
        <div class="breadcrumb top20">
          <a href="<?php echo Util::caminho_projeto(); ?>/"><i class="fa fa-home"></i></a>
          <a class="active">DICAS</a>
        </div>
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- TITULO BANNER E DESCRICAO -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 titulo_contatos padding0 font-futura  bottom50 top35 text-right">
          <h5>CONFIRA AS NOSSAS DICAS</h5>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO BANNER  E DESCRICAO  -->
        <!-- ======================================================================= -->




          <!-- ======================================================================= -->
          <!--DICAS GERAL -->
          <!-- ======================================================================= -->
          <?php
          $result = $obj_site->select("tb_dicas","order by rand()");
          if (mysql_num_rows($result) > 0) {
            $i = 0;
            while($row = mysql_fetch_array($result)){
              ?>

              <div class="col-xs-12 dicas_geral top50">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",669, 262, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  <div class="top20">
                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                  </div>
                  <div class="top25 dicas_geral_desc">
                    <p><?php Util::imprime($row[descricao],500); ?></p>
                  </div>
                </a>

                <div class="col-xs-3 padding0 top25">
                  <a class="btn btn_dicas col-xs-12" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="SAIBA MAIS">SAIBA MAIS</a>
                </div>

              </div>

              <?php
              if ($i == 3) {
                echo '<div class="clearfix"></div>';
                $i = 0;
              }else{
                $i++;
              }
            }
          }
          ?>

          <!-- ======================================================================= -->
          <!--DICAS GERAL -->
          <!-- ======================================================================= -->





      </div>

      <div class="col-xs-5  top15">
        <!--  ==============================================================  -->
        <!-- bg lateral ->
        <!-  ==============================================================  -->
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/bg_lateral_dicas.png" alt="" />
        <!--  ==============================================================  -->
        <!-- bg lateral ->
        <!- ==============================================================  -->

        <!-- ======================================================================= -->
        <!-- MENU CATEGORIA UNIFORMES -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/categorias_uniformes.php'); ?>
        <!-- ======================================================================= -->
        <!-- MENU CATEGORIA UNIFORMES -->
        <!-- ======================================================================= -->

      </div>




    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
