
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>





</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 218px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12">
				<div class="breadcrumb top20">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile"><i class="fa fa-home"></i></a>
					<a class="active">PRODUTOS</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->




	<div class='container'>
		<div class="row">

			<div class="col-xs-12 padding0">

				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->
				<div class="col-xs-6 titulo_produtos font-futura top35 ">
					<h5>CONFIRA TODOS OS NOSSOS <span>PRODUTOS</span></h5>
				</div>
				<!-- ======================================================================= -->
				<!-- TITULO BANNER  E DESCRICAO  -->
				<!-- ======================================================================= -->
			</div>
		</div>
	</div>


	<!-- ======================================================================= -->
	<!-- PRODUTOS  GERAL -->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row">
			<div class="col-xs-6">
				<a class="btn btn_produtos col-xs-12 top5" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos-categoria">
					VER TODOS OS PRODUTOS
				</a>
			</div>

			<div class="clearfix">  </div>


			<!-- ======================================================================= -->
			<!-- TODOS OS PRODUTOS   -->
			<!-- ======================================================================= -->
			<?php require_once('../includes/menu_produtos.php'); ?>
			<!-- ======================================================================= -->
			<!-- TODOS OS PRODUTOS   -->
			<!-- ======================================================================= -->


			<!-- ======================================================================= -->
			<!-- TODOS OS PRODUTOS   -->
			<!-- ======================================================================= -->
			<div class="col-xs-6 top25">
				<div class="fundo_produtos_todos pb15 text-center">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos-categoria">
						<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_produto07.png" alt="" />
						<div class="top20">
							<h2>VER TODOS OS PRODUTOS</h2>
						</div>
					</a>
				</div>
			</div>
			<!-- ======================================================================= -->
			<!-- TODOS OS PRODUTOS   -->
			<!-- ======================================================================= -->


		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- PRODUTOS  GERAL    -->
	<!-- ======================================================================= -->






	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>
