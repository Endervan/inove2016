<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>
</head>

<body>
  <?php require_once('./includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- slider	-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                <div class="carousel-caption text-center">
                  <?php /*<h1><?php Util::imprime($imagem[titulo]); ?></h1>
                  <p><?php Util::imprime($imagem[legenda]); ?></p>*/ ?>
                  <?php if (!empty($imagem[url])) { ?>
                    <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                    <?php } ?>
                  </div>
                </div>

                <?php
                $i++;
              }
            }
            ?>


          </div>



          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>



        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- slider	-->
    <!-- ======================================================================= -->





    <div class="container">
      <div class="row subir_produtos_destaques">
        <!-- ======================================================================= -->
        <!-- conheca escritorio    -->
        <!-- ======================================================================= -->
        <div class="col-xs-8 bottom80 lato-black">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class=" btn_linha_escritorio">
            CONHECER TODA LINHA DE UNIFORMES
          </a>
        </div>
        <!-- ======================================================================= -->
        <!-- conheca escritorio    -->
        <!-- ======================================================================= -->
        <div class="clearfix">  </div>

        <!-- ======================================================================= -->
        <!--PRODUTOS EM DESTAQUES    -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 lato-black">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn_produtos_destaques btn-lg active" role="button">PRODUTOS EM DESTAQUE</a>
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn_produtos_destaques btn-lg" role="button">PRODUTOS EM PROMOÇÕES </a>
          <!-- <a href="<?php //echo Util::caminho_projeto() ?>/produtos" class="btn btn_produtos_destaques btn-lg" role="button">PRODUTOS MAIS VENDIDOS</a> -->

        </div>
        <!-- ======================================================================= -->
        <!-- PRODUTOS EM DESTAQUES    -->
        <!-- ======================================================================= -->
      </div>
    </div>



    <!-- ======================================================================= -->
    <!--PRODUTOS home    -->
    <!-- ======================================================================= -->
    <div class="container top20">
      <div class="row">
        <?php
        $result = $obj_site->select("tb_produtos","order by rand() limit 6");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while($row = mysql_fetch_array($result)){
            $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
            $row_categoria = mysql_fetch_array($result_categoria);
            ?>
            <div class="col-xs-6 produtos_home top30">
              <h3 class="text-right"><?php Util::imprime($row_categoria[titulo]); ?></h3>
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",263, 333, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>

              <div class="top15">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>

              <div class="cod_numerao top5">
                <p>Cód.<?php Util::imprime($row[codigo_produto],100); ?></p>
              </div>

              <div class="col-xs-8 padding0">
                <a class="btn btn_saiba_mais_produtos col-xs-12" href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip"  title="<?php Util::imprime($row[url_amigavel]); ?>">SAIBA MAIS</a>
              </div>

              <div class="col-xs-4">
                <a class="btn btn_saiba_mais_produtos col-xs-12" href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'" data-toggle="tooltip"  title="Adicionar ao orçamento">
                  <img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/carrinho_produtos.png" alt="">
                </a>
              </div>

            </div>

            <?php
            if ($i == 3) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }
          }
        }
        ?>


        <div class="clearfix"></div>
        <div class="col-xs-12 top25">
          <a class="btn btn_ver_todos col-xs-offset-2 col-xs-8" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>"  title="VER TODOS OS PRODUTOS">VER TODOS OS PRODUTOS</a>
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- PRODUTOS home    -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--CONHECA MAIS   -->
    <!-- ======================================================================= -->
    <div class="container bg_conheca_home top20">
      <div class="row">



        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

        <div class="col-xs-8 col-xs-offset-4 top60">
          <div class="conheca_mais_home bottom15">
            <p><?php Util::imprime($row[descricao],1000); ?></p>
          </div>

          <div class="col-xs-6 padding0 top15">
            <a class="btn btn_conheca_mais col-xs-12" href="<?php echo Util::caminho_projeto() ?>/empresa"  title="SAIBA MAIS">SAIBA MAIS</a>
          </div>



        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- CONHECA MAIS   -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--NOSSA DICAS -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">
        <div class="col-xs-4 text-center top20">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/nossas_dicas_seta.png" alt="" />
        </div>


        <div class="col-xs-8 text-right subir_dicas padding0">
          <div class="col-xs-8  col-xs-offset-4 top15">
            <a class="btn btn_nossas_dicas col-xs-12" href="<?php echo Util::caminho_projeto() ?>/dicas/<?php Util::imprime($row[url_amigavel]); ?>"  title="VER TODAS AS DICAS">
              VER TODAS AS DICAS <i class="fa fa-angle-right left10"></i>
            </a>
          </div>

          <!-- ======================================================================= -->
          <!--SLIDER DICAS -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_dicas.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER DICAS -->
          <!-- ======================================================================= -->
        </div>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSA DICAS -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!--NOSSOS CLIENTES -->
    <!-- ======================================================================= -->
    <div class="container">
      <div class="row">



        <div class="col-xs-8 padding0">

          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_clientes.php'); ?>
          <!-- ======================================================================= -->
          <!--SLIDER CLIENTES -->
          <!-- ======================================================================= -->
        </div>

        <div class="col-xs-4 text-center top40">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/nosso_clientes_home.png" alt="" />
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- NOSSOS CLIENTES -->
    <!-- ======================================================================= -->




    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->

  </body>

  </html>







  <!-- slider JS files -->
  <script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
  <script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


  <script>
  jQuery(document).ready(function($) {
    // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
    // it's recommended to disable them when using autoHeight module
    $('#content-slider-1').royalSlider({
      autoHeight: true,
      arrowsNav: true,
      arrowsNavAutoHide: false,
      keyboardNavEnabled: true,
      controlNavigationSpacing: 0,
      controlNavigation: 'tabs',
      autoScaleSlider: false,
      arrowsNavAutohide: true,
      arrowsNavHideOnTouch: true,
      imageScaleMode: 'none',
      globalCaption:true,
      imageAlignCenter: false,
      fadeinLoadedSlide: true,
      loop: false,
      loopRewind: true,
      numImagesToPreload: 6,
      keyboardNavEnabled: true,
      usePreloader: false,
      autoPlay: {
        // autoplay options go gere
        enabled: true,
        pauseOnHover: true
      }

    });
  });
  </script>


  <?php require_once('./includes/js_css.php') ?>
