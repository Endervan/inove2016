
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>





</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 218px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12">
				<div class="breadcrumb top20">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile"><i class="fa fa-home"></i></a>
					<a class="active">GALERIA</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->



	<div class="container">
		<div class="row">
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->
			<div class="col-xs-10 top80 titulo_cat font-futura text-right">
				<h5>CONFIRA TODAS AS</h5>
				<h5><span>NOSSAS GALERIAS</span></h5>

			</div>
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->


			<div class="col-xs-12">
				<a href="javascript:void(0);" title="" class="top5"  data-toggle="modal" data-target="#myModal_galerias">
						<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/menu_galeria_modal.png" alt="">
				</a>
			</div>


			<!-- ======================================================================= -->
			<!-- MODAL GALERIA GERAL  -->
			<!-- ======================================================================= -->
			<div class="modal fade" id="myModal_galerias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Nossas Galerias</h4>
			      </div>
			      <div class="modal-body">

			          <?php
			          $result = $obj_site->select("tb_galerias");
			          if (mysql_num_rows($result) > 0) {
			            while($row = mysql_fetch_array($result)){
			              ++$i;
			            ?>
			                 <a href="<?php echo Util::caminho_projeto() ?>/mobile/galeria/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item"><?php Util::imprime($row[titulo]); ?></a>

			            <?php
			            }
			          }
			          ?>

			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- ======================================================================= -->
			<!-- MODAL GALERIA GERAL  -->
			<!-- ======================================================================= -->




			<!-- ======================================================================= -->
			<!-- SLIDER GALERIAS   -->
			<!-- ======================================================================= -->
			<div class="col-xs-12">
				<?php //require_once('../includes/slider_galeria.php') ?>
				<div class="borda_galeria"></div>
			</div>
			<!-- ======================================================================= -->
			<!-- SLIDER GALERIAS   -->
			<!-- ======================================================================= -->



		</div>
	</div>






	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>
