
<!-- Bootstrap core CSS -->
<link href="<?php echo Util::caminho_projeto(); ?>/css/bootstrap.min.css" rel="stylesheet">




<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="<?php echo Util::caminho_projeto(); ?>/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo Util::caminho_projeto() ?>/js/ie10-viewport-bug-workaround.js"></script>



<link href="<?php echo Util::caminho_projeto(); ?>/mobile/css/mobile.css" rel="stylesheet" type="text/css" media="screen" />

<script type="text/javascript">
    $(document).ready(function () {
        $('.central-atendimento-menu').hide();

        $('.btn-central-atendimento').click(function () {
            $('.central-atendimento-menu').toggle();
        });
    });
</script>

<script>
	$(function(){
      // bind change event to select
      $('#menu, #nossas-categorias, #categorias ,#menu-site').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>








<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrapValidator.min.css"/>
 <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-lightbox.min.js"></script>





<!--    ====================================================================================================     -->
<!--    CLASSE DE FONTS DO SITE   -->
<!--    ====================================================================================================     -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/fonts/font-awesome-4.6.3/css/font-awesome.min.css">





<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
    window.location = '<?php echo Util::caminho_projeto() ?>/mobile/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>


<script>
  $(function(){
      // bind change event to select
      $('#menu-site').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>







<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/js/jquery.flexslider-min.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.js"></script>
<script defer src="<?php echo Util::caminho_projeto() ?>/js/demo.js"></script>


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/flexslider.css" type="text/css" media="screen" />



<!-- Rating
http://plugins.krajee.com/star-rating
================================================== -->
<link href="<?php echo Util::caminho_projeto() ?>/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo Util::caminho_projeto() ?>/js/star-rating.min.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function () {

    $('.avaliacao').rating({
          min: 0,
          max: 5,
          step: 1,
          size: 'xs',
          showClear: false,
          disabled: true,
          clearCaption: 'Seja o primeiro a avaliar.',
          starCaptions: {
                            0.5: 'Half Star',
                            1: 'Ruim',
                            1.5: 'One & Half Star',
                            2: 'Regular',
                            2.5: 'Two & Half Stars',
                            3: 'Bom',
                            3.5: 'Three & Half Stars',
                            4: 'Ótimo',
                            4.5: 'Four & Half Stars',
                            5: 'Excelente'
                        }
       });


});
</script>











<!-- ======================================================================= -->
<!-- CAEGORIAS E SUB-CATEGORIAS  -->
<!-- ======================================================================= -->
<div class="modal fade" id="myModal_categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nossas Categorias</h4>
      </div>
      <div class="modal-body">

          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
              ++$i;
            ?>
                 <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos-categoria/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item"><?php Util::imprime($row[titulo]); ?></a>


                  <?php
                    $result1 = $obj_site->select("tb_subcategorias_produtos", "and id_categoriaproduto = $row[0] ");
                    if (mysql_num_rows($result1) > 0) {
                      while($row1 = mysql_fetch_array($result1)){
                      ?>
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos-categoria/<?php Util::imprime($row[url_amigavel]); ?>/<?php Util::imprime($row1[url_amigavel]); ?>" class="list-group-item">
                          <i class="fa fa-angle-double-right left10" aria-hidden="true"></i>
                          <?php Util::imprime($row1[titulo]); ?>
                        </a>
                      <?php
                      }
                    }
                    ?>

            <?php
            }
          }
          ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- CAEGORIAS E SUB-CATEGORIAS  -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- scroll personalizado    -->
<!-- ======================================================================= -->

<!-- the mousewheel plugin - optional to provide mousewheel support -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.min.js"></script>

<!-- the jScrollPane script -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/jquery.jscrollpane.min.js"></script>


<script type="text/javascript" >
$(function() {
  $('.scrol_empresa').jScrollPane();
});

$(function() {
  $('.lista_produtos .list-group').jScrollPane();
});
</script>
<!-- ======================================================================= -->
<!-- scroll personalizado    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- colobox    -->
<!-- ======================================================================= -->

<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/js/colorbox/example1/colorbox.css" />
<script src="<?php echo Util::caminho_projeto() ?>/js/colorbox/jquery.colorbox-min.js"></script>
<script>
$(document).ready(function(){
  //Examples of how to assign the Colorbox event to elements
  $(".group1").colorbox({rel:'group1'});
  $(".group2").colorbox({rel:'group2', transition:"fade"});
  $(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
  $(".group4").colorbox({rel:'group4', slideshow:true});
  $(".ajax").colorbox();
  $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
  $(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
  $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
  $(".inline").colorbox({inline:true, width:"50%"});
  $(".callbacks").colorbox({
    onOpen:function(){ alert('onOpen: colorbox is about to open'); },
    onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
    onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
    onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
    onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
  });

  $('.non-retina').colorbox({rel:'group5', transition:'none'})
  $('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});

  //Example of preserving a JavaScript event for inline calls.
  $("#click").click(function(){
    $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
    return false;
  });
});
</script>
<!-- ======================================================================= -->
<!-- colobox    -->
<!-- ======================================================================= -->




<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4qyhcRDsjaqsi2d5TqiGwKt6yUgBxu6X";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
