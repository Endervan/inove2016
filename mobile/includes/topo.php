<div class="container topo_geral">
  <div class="row relativo">


    <div class="col-xs-8 col-xs-offset-4 top10 padding0">
      <!-- ======================================================================= -->
      <!-- UNIDADES  falta fazer ajax pra troca os numeros de acordo com unidade selecionada -->
      <!-- ======================================================================= -->
      <div class="unidades">
        <div class="col-xs-4 text-right padding0 top10">  <span>Estou em:</span></div>
        <select class="form-control text-center" id="appearance-select1" onchange="javascript:location.href = this.value;">

          <?php
          $result = $obj_site->select("tb_unidades","limit 5");
          if (mysql_num_rows($result) > 0) {
            $i = 0;
            while($row = mysql_fetch_array($result)){
              ?>

              <option value="?unidade=<?php echo $row[url_amigavel] ?>" <?php if($config[idunidade] == $row[idunidade] ){ echo 'selected'; } ?> class="btn btn_outras_unidades" ><?php echo Util::imprime($row[titulo]); ?></option>
              <?php

            }
          }
          ?>
        </select>
      </div>
      <!-- ======================================================================= -->
      <!-- UNIDADES   -->
      <!-- ======================================================================= -->
    </div>




    <div class="col-xs-12 top5">

      <div class="bg_amarelo_topo">
        <!--  ==============================================================  -->
        <!-- logo -->
        <!--  ==============================================================  -->
        <div class="logo_topo">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
          </a>
        </div>
        <!--  ==============================================================  -->
        <!-- logo -->
        <!--  ==============================================================  -->



        <div class="col-xs-offset-3 col-xs-9 top10 bottom10 menu_topo">



          <!-- contatos topo  -->
          <div class="media pull-right top5">
            <div class="media-left media-middle lato_bold">
              <h4 class="media-heading top5">	<span><i class="fa fa-phone" aria-hidden="true"></i><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?></h4>
            </div>
            <div class="media-body">
              <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.jpg" alt="">
              </a>
            </div>
          </div>
          <!-- contatos topo  -->


          <?php if(!empty($config[telefone2])): ?>
            <!-- contatos topo  -->
            <div class="media  pull-right top5">
              <div class="media-left media-middle lato_bold">
                <h4 class="media-heading top5">	<span> <i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?></h4>
              </div>
              <div class="media-body">
                <a href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.jpg" alt="">
                </a>
              </div>
            </div>
            <!-- contatos topo  -->
          <?php endif; ?>

          <?php if(!empty($config[telefone4])): ?>
            <!-- contatos topo  -->
            <div class="media  pull-right top5">
              <div class="media-left media-middle lato_bold">
                <h4 class="media-heading top5">	<span> <i class="fa fa-whatsapp" aria-hidden="true"></i> <?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?></h4>
              </div>
              <div class="media-body">
                <a href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.jpg" alt="">
                </a>
              </div>
            </div>
            <!-- contatos topo  -->
          <?php endif; ?>

        </div>
      </div>


    </div>


  </div>
</div>

<div class="container topo_geral">
  <div class="row">

    <div class="col-xs-12 bottom10">
      <div class="">

        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->
        <div class="col-xs-8 bg_branco_topo text-right">
          <select name="menu" id="menu-site" class="select-menu">
            <option value=""></option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa">A EMPRESA</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/galerias">GALERIA</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/medidas">MEDIDAS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</option>
            <option value="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</option>
          </select>
        </div>
        <!-- ======================================================================= -->
        <!-- MENU  -->
        <!-- ======================================================================= -->

        <!-- ======================================================================= -->
        <!-- botao carrinho de compra -->
        <!-- ======================================================================= -->
        <div class="col-xs-4 dropdown bg_branco_topo text-right padding0">
          <a class="btn btn_carrinho" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/carrinho_topo.png" alt="">
          </a>

          <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

            <?php
            if(count($_SESSION[solicitacoes_produtos]) > 0)
            {
              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
                <div class="lista-itens-carrinho">
                  <div class="col-xs-2">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                  </div>
                  <div class="col-xs-8">
                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                  </div>
                  <div class="col-xs-1">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                  </div>
                  <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                </div>
                <?php
              }
            }





            if(count($_SESSION[solicitacoes_servicos]) > 0)
            {


              for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
              {
                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                ?>
                <div class="lista-itens-carrinho">
                  <div class="col-xs-2">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
                  </div>
                  <div class="col-xs-8">
                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                  </div>
                  <div class="col-xs-1">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                  </div>
                  <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                </div>
                <?php
              }
            }

            ?>

            <div class="col-xs-12 text-right top10 bottom20">
              <a class="btn btn_amarelo" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="ENVIAR ORÇAMENTO" >
                ENVIAR ORÇAMENTO
              </a>
            </div>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- botao carrinho de compra -->
        <!-- ======================================================================= -->


      </div>
    </div>
  </div>
</div>
