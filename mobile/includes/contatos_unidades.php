
<!--  ==============================================================  -->
<!-- UNIDADES-->
<!--  ==============================================================  -->
<div class="col-xs-6  unidades_contatos top20">
	<div class="media">
		<div class="media-left media-middle ">
			<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/unidades_contatos.jpg" alt="">
		</div>
		<div class="media-body">
			<h1 class="media-heading lato-bold">
				<?php echo Util::imprime($config[titulo]); ?>
			</h1>
		</div>
	</div>

</div>

<div class="col-xs-6 ">
	<div class="top15 unidades_contatos">
		<div class="dropdown font-futura ">
			<button class="btn btn_outras_geral  col-xs-12 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				SELECIONAR OUTRA UNIDADE
				<span class="fa fa-angle-down left10"></span>
			</button>
			<ul class="dropdown-menu col-xs-offset-2 col-xs-10" aria-labelledby="dropdownMenu1">
				<?php
		          $result = $obj_site->select("tb_unidades","limit 5");
		          if (mysql_num_rows($result) > 0) {
		            $i = 0;
		            while($row = mysql_fetch_array($result)){
		              ?>
					<li><a class="btn btn_outras_unidades" href="?unidade=<?php echo $row[url_amigavel] ?>"><?php Util::imprime($row[titulo]); ?></a></li>
		              <?php
			         }
			    }
			    ?>
			</ul>
		</div>
	</div>
</div>
<!--  ==============================================================  -->
<!-- UNIDADES-->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- CONTATOS-->
<!--  ==============================================================  -->
<div class="col-xs-8 contatos_topo  top10">
	<div class="media pull-right">
		<div class="media-left media-middle ">
			<h2 class="media-heading lato-bold">
				<span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
			</h2>

		</div>
		<div class="media-body">
			<a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
				<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone_contatos.jpg" alt="">
			</a>
		</div>
	</div>

	<?php if (!empty($config[telefone2])):  ?>

		<div class="media pull-right">
			<div class="media-left media-middle ">
				<h2 class="media-heading lato-bold">
					<span><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
				</h2>

			</div>
			<div class="media-body">
				<a href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
					<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone_contatos.jpg" alt="">
				</a>
			</div>
		</div>

	<?php endif; ?>


</div>
<!--  ==============================================================  -->
<!-- CONTATOS-->
<!--  ==============================================================  -->

<div class="clearfix">	</div>
