
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once("../includes/head.php"); ?>


</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 175px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->





	<div class='container '>
		<div class="row">

			<div class="col-xs-12 padding0">
				<!-- ======================================================================= -->
				<!-- Breadcrumbs    -->
				<!-- ======================================================================= -->
				<div class="breadcrumb top20">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile"><i class="fa fa-home"></i></a>
					<a class="active">FALE CONOSCO</a>
				</div>
				<!-- ======================================================================= -->
				<!-- Breadcrumbs    -->
				<!-- ======================================================================= -->

				<!-- ======================================================================= -->
				<!-- TITULO BANNER E DESCRICAO -->
				<!-- ======================================================================= -->
				<div class="col-xs-11 col-xs-offset-1 titulo_contatos font-futura  top35 text-center">
					<h5>SEMPRE QUE PRECISAR, FALE CONOSCO</h5>
				</div>
				<!-- ======================================================================= -->
				<!-- TITULO BANNER  E DESCRICAO  -->
				<!-- ======================================================================= -->
				</div>

				<div class="col-xs-12  top20 ">
					<!-- ======================================================================= -->
					<!-- CONTATOS e UNIDADES-->
					<!-- ======================================================================= -->
					<?php require_once('../includes/contatos_unidades.php') ?>
					<!-- ======================================================================= -->
					<!-- CONTATOS e UNIDADES-->
					<!-- ======================================================================= -->
				</div>

				<div class="col-xs-12 bg_lateral padding0">
					<!--  ==============================================================  -->
					<!-- FORMULARIO-->
					<!--  ==============================================================  -->
					<form class="form-inline FormContatos" role="form" method="post" enctype="multipart/form-data">
						<div class="fundo_formulario">
							<!-- formulario orcamento -->

								<div class="col-xs-8 top15">
									<div class="form-group input100 has-feedback">
										<input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
										<span class="fa fa-user form-control-feedback top15"></span>
									</div>
									</div>


								<div class="col-xs-8 top15">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
										<span class="fa fa-envelope form-control-feedback top15"></span>
									</div>
								</div>





								<div class="col-xs-8 top15">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
										<span class="fa fa-phone form-control-feedback top15"></span>
									</div>
								</div>

								<div class="col-xs-8 top15">
									<div class="form-group  input100 has-feedback">
										<input type="text" name="localidade" class="form-control fundo-form1 input-lg input100" placeholder="LOCALIDADE">
										<span class="fa fa-home form-control-feedback"></span>
									</div>
								</div>


								<div class="col-xs-8 top15">
									<div class="form-group input100 has-feedback">
										<textarea name="mensagem" cols="25" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
										<span class="fa fa-pencil form-control-feedback top15"></span>
									</div>
								</div>

								<div class="col-xs-8 top10 text-right">
									<div class="bottom25">
										<button type="submit" class="btn btn-formulario" name="btn_contato">
											ENVIAR
										</button>
									</div>
								</div>


						</div>
					</form>
					<!--  ==============================================================  -->
					<!-- FORMULARIO-->
					<!--  ==============================================================  -->
				</div>

				<div class="clearfix"></div>

				<div class="col-xs-12 top60">
					<img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/saiba_como_chegar_contatos.jpg" alt="" />
				</div>


				<div class="col-xs-12 pt15 top20">
					<div class="bg_cinza">
						<div class="col-xs-5 text-center padding0">
							<h1>UNIDADE DE <?php echo Util::imprime($config[titulo]); ?></h1>
						</div>
						<div class="col-xs-7">
							<p><?php echo Util::imprime($config[endereco]); ?></p>
						</div>
					</div>
				</div>


				<div class="col-xs-12">
					<!-- ======================================================================= -->
					<!-- mapa   -->
					<!-- ======================================================================= -->
					<iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="508" frameborder="0" style="border:0" allowfullscreen></iframe>

					<!-- ======================================================================= -->
					<!-- mapa   -->
					<!-- ======================================================================= -->
				</div>

		</div>
	</div>




	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->



</body>

</html>


<?php require_once('../includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
	$texto_mensagem = "
	Nome: ".($_POST[nome])." <br />
	Localidade: ".($_POST[localidade])." <br />
	Telefone: ".($_POST[telefone])." <br />
	Email: ".($_POST[email])." <br />
	Mensagem: <br />
	".(nl2br($_POST[mensagem]))."
	";


	Util::envia_email($config_email[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
	Util::envia_email($config_email[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

	Util::alert_bootstrap("Obrigado por entrar em contato.");
	unset($_POST);
}


?>



<script>
$(document).ready(function() {
	$('.FormContatos').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'fa fa-check',
			invalid: 'fa fa-remove',
			validating: 'fa fa-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {
						message: 'Insira seu nome.'
					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'Informe um email.'
					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {
						message: 'Por favor informe seu numero!.'
					},
					phone: {
						country: 'BR',
						message: 'Informe um telefone válido.'
					}
				},
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>
