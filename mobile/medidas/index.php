
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('../includes/head.php'); ?>





</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
<style>
.bg-interna{
	background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 218px center  no-repeat;
}
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/topo.php') ?>
	<!-- ======================================================================= -->
	<!-- topo    -->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->
	<div class='container '>
		<div class="row">
			<div class="col-xs-12">
				<div class="breadcrumb top20">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile"><i class="fa fa-home"></i></a>
					<a class="active">MEDIDAS</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- Breadcrumbs    -->
	<!-- ======================================================================= -->



	<div class="container">
		<div class="row">
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->
			<div class="col-xs-offset-2 col-xs-10 titulo_cat font-futura text-right">
				<h5>CONFIRA NOSSA</h5>
				<h5><span>TABELA DE MEDIDAS E CORES</span></h5>

			</div>
			<!-- ======================================================================= -->
			<!-- TITULO BANNER E DESCRICAO -->
			<!-- ======================================================================= -->

		</div>
	</div>



	<div class="container">
	    <div class="row top10">
	      <div class="col-xs-12">
	          
	          <a href="javascript:void(0);"><h1 class="titulo-categoria-medidas" data-toggle="modal" data-target="#myModal">SELECIONE A CATEGORIA DESEJADA</h1></a>
	          
			
			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Selecione a categoria desejada</h4>
			      </div>
			      <div class="modal-body">
			        	<div class="lista-link-medidas">
			                <?php 
			                $url_amigavel = $_GET[get1];
			                if(!empty($url_amigavel)){
			                  $idmedida = $obj_site->get_id_url_amigavel("tb_medidas", "idmedida", $url_amigavel);
			                }

			                $result = $obj_site->select("tb_medidas");
			                if (mysql_num_rows($result) > 0) {
			                  $i = 0;
			                  while($row = mysql_fetch_array($result)){
			                    
			                    //  verifico se tem url
			                    if(empty($url_amigavel) and $i == 0){
			                      $dados = $row;
			                      $i = 1;
			                      $active = 'active';
			                    }elseif($idmedida == $row[idmedida]){
			                      $dados = $row;
			                      $active = 'active';
			                    }
			                  ?>
			                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/medidas/<?php Util::imprime($row[url_amigavel]); ?>" class="<?php echo $active; ?>"><?php Util::imprime($row[titulo]); ?></a>        
			                  <?php 
			                    $active = '';
			                  }
			                }
			                ?>
			          </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			      </div>
			    </div>
			  </div>
			</div>



	          

	      </div>


	      <div class="col-xs-12 detalhes-medidas">
	          

	          <h1><span><?php Util::imprime($dados[titulo]); ?></span></h1>
	          
	          <?php if(!empty($dados[imagem])){ ?>
	            <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados[imagem]); ?>" alt="" class="right20 pull-left">
	          <?php } ?>

	          <p class="bottom30"><?php Util::imprime($dados[descricao]); ?></p>
	          <div class="clearfix"></div>
	          <br>


	          <div class="table-responsive">
	          	<?php echo($dados[tabelas]); ?>
	          </div>



	      </div>


	    </div>
	  </div>

	


	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- ======================================================================= -->
	<!-- rodape    -->
	<!-- ======================================================================= -->


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>
