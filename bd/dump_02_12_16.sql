CREATE TABLE `tb_medidas` (
  `idmedida` INT NOT NULL,
  `titulo` VARCHAR(255) NULL,
  `descricao` LONGTEXT NULL,
  `tabelas` LONGTEXT NULL,
  `imagem` VARCHAR(255) NULL,
  `ativo` VARCHAR(3) NULL DEFAULT 'SIM',
  `ordem` INT NULL,
  `url_amigavel` VARCHAR(255) NULL,
  PRIMARY KEY (`idmedida`));


ALTER TABLE `tb_medidas` 
CHANGE COLUMN `idmedida` `idmedida` INT(11) NOT NULL AUTO_INCREMENT ;
