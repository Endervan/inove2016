<?php
ob_start();
session_start();

class Medida_Model extends Dao
{

	private $nome_tabela = "tb_medidas";
	private $chave_tabela = "idmedida";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
            $this->obj_imagem = new Imagem();
            parent::__construct();
	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

            if(count($ordem) > 0):

                foreach($ordem as $key=>$orde):

                    $sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
                    parent::executaSQL($sql);

                endforeach;

            endif;

	}


	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
        $obj_jquery = new Biblioteca_Jquery();
        ?>



        <div class="col-xs-12 form-group ">
            <label>Título</label>
            <input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
        </div>





        <div class="col-xs-12 form-group ">
            <label>Imagem da medida</label>
            <?php
            if(!empty($dados[imagem])){
            ?>
                <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
                <label>Atual:</label>
                <div class="clearfix"></div>
                <img class="col-xs-2" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]) ?>" />
            <?php } else { ?>
                <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
            <?php } ?>
        </div>




        <div class="col-xs-12 form-group ">
            <label>Descrição</label>
            <?php $obj_jquery->ckeditor('descricao', $dados[descricao]); ?>
        </div>



        <div class="col-xs-12 form-group ">
            <label>Tabelas de Medidas</label> 

            <a class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Ajuda</a>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Como inserir as medidas</h4>
                  </div>
                  <div class="modal-body">
                    Abaixo segue o tutorial de como inseriri as medidas.

                    <br><br> 1 - Copie o código abaixo.
                    
                    <br><br>

                  
<pre>
&lt;div class="titulo-tabela">CAMISAS&lt;/div>
&lt;table class="table table-bordered"> 
    &lt;thead> 
      &lt;tr> 
        &lt;th>#&lt;/th> 
        &lt;th>First Name&lt;/th> 
        &lt;th>Last Name&lt;/th> 
        &lt;th>Username&lt;/th> 
      &lt;/tr> 
    &lt;/thead> 
    &lt;tbody> 
      &lt;tr>
        &lt;td>1&lt;/td>
        &lt;td>2&lt;/td>
        &lt;td>3&lt;/td>
        &lt;td>4&lt;/td>
      &lt;/tr>
    &lt;/tbody>
  &lt;/table>
</pre>  

                    <br> 2 - Clique no botão "Source", e cole o código, após isso clique novamente no botão "Source" para voltar para o modo visual
                    
                    <br><br> 3 - Agora pode editar a tabela como desejar.

                    <br><br>
                   

    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                  </div>
                </div>
              </div>
            </div>


            <div class="clearfix"></div>
            <div class="top10"></div>
            

            <?php $obj_jquery->ckeditor('tabelas', $dados[tabelas]); ?>
        </div>











        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->
        <?php //require_once("../includes/google_seo.php"); ?>
        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->





        <!-- ======================================================================= -->
        <!-- VALIDACAO DO FORMULARIO    -->
        <!-- ======================================================================= -->
        <script>
          $(document).ready(function() {
            $('.FormPrincipal').bootstrapValidator({
              message: 'This value is not valid',
              feedbackIcons: {
								valid: 'fa fa-check',
								invalid: 'fa fa-remove',
								validating: 'fa fa-refresh'
              },
              fields: {


              titulo: {
                validators: {
                  notEmpty: {

                  }
                }
              },

              id_categoriaproduto: {
                validators: {
                  notEmpty: {

                  }
                }
              },


              marca: {
                validators: {
                  notEmpty: {

                  }
                }
              },

            id_subcategoriaproduto: {
                validators: {
                  notEmpty: {

                  }
                }
              },


              email: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                }
              },

              mensagem: {
                validators: {
                  notEmpty: {

                  }
                }
              }
            }
          });
          });
        </script>




            <?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_arquivo("../../uploads", $_FILES[imagem]);
            }

            //  VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem_interna][name] != "")
            {
                //  EFETUO O UPLOAD DA IMAGEM_interna
                $dados[imagem_interna] = Util::upload_imagem("../../uploads", $_FILES[imagem_interna]);
            }


            //	CADASTRA O USUARIO
            $id = parent::insert($this->nome_tabela, $dados);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


            Util::script_msg("Cadastro efetuado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_arquivo("../../uploads", $_FILES[imagem]);
            }

            //  VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem_interna][name] != "")
            {
                //  EFETUO O UPLOAD DA IMAGEM_interna
                $dados[imagem_interna] = Util::upload_imagem("../../uploads", $_FILES[imagem_interna]);
            }

            parent::update($this->nome_tabela, $id, $dados);


            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



            Util::script_msg("Alterado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}




	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
            if($ativo == "SIM")
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
            else
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
            //	BUSCA OS DADOS
            $row = $this->select($id);

            $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
            if($id != "")
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            WHERE
                                    " . $this->chave_tabela. " = '$id'
                            ";
                    return mysql_fetch_array(parent::executaSQL($sql));
            }
            else
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            ORDER BY
                                    ordem desc
                            ";
                    return parent::executaSQL($sql);
            }

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
            $obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
            $obj_imagem->redimensiona($largura, $altura, $tipo);
            $obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
