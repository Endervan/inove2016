<?php
ob_start();
session_start();

class Produto_Model extends Dao
{

	private $nome_tabela = "tb_produtos";
	private $chave_tabela = "idproduto";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
            $this->obj_imagem = new Imagem();
            parent::__construct();
	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

            if(count($ordem) > 0):

                foreach($ordem as $key=>$orde):

                    $sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
                    parent::executaSQL($sql);

                endforeach;

            endif;

	}


	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
        $obj_jquery = new Biblioteca_Jquery();
        ?>

        <script type="text/javascript" language="javascript">
            jQuery(function($){
                $(".moeda").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
            });
        </script>


        <script type="text/javascript">
        $(function() {
            $('#id_categoriaproduto').change(function(){
                $('#id_subcategoriaproduto').load('<?php echo Util::caminho_projeto() ?>/includes/carrega_subcategoria_produtos.php?id='+$('#id_categoriaproduto').val());
            });
        });
        </script>





        <div class="col-xs-5 form-group ">
            <label>Categoria</label>

             <?php  Util::cria_select_bd("tb_categorias_produtos", "idcategoriaproduto", "titulo", "id_categoriaproduto", $dados[id_categoriaproduto], "form-control fundo-form1 input100") ?>
        </div>


        <div class="col-xs-5 form-group ">
            <label>Sub-Categoria</label>
            <?php
	            $sql = "SELECT * FROM tb_subcategorias_produtos WHERE id_categoriaproduto = '$dados[id_categoriaproduto]'";
            Util::cria_select_bd_com_sql('id_subcategoriaproduto', $dados[id_subcategoriaproduto], $sql, "", "form-control fundo-form1 input100");
            ?>
        </div>


        <div class="col-xs-2 form-group ">
            <label>Exibir na home</label>
            <?php echo Util::get_sim_nao("exibir_home", $dados[exibir_home], 'form-control fundo-form1 input100') ?>
        </div>

				<div class="clearfix">	</div>



        <div class="col-xs-6 form-group ">
            <label>Título</label>
            <input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
        </div>


        <div class="col-xs-6 form-group ">
            <label>Código</label>
            <input type="text" name="codigo_produto" value="<?php echo Util::imprime($dados[codigo_produto]) ?>" class="form-control fundo-form1 input100" >
        </div>




        <div class="col-xs-12 form-group ">
            <label>Imagem da capa</label>
            <?php
            if(!empty($dados[imagem])){
            ?>
                <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
                <label>Atual:</label>
                <div class="clearfix"></div>
                <img class="col-xs-2" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]) ?>" />
            <?php } else { ?>
                <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
            <?php } ?>
        </div>



        <div class="col-xs-12 form-group ">
            <label>Descrição</label>
            <?php $obj_jquery->ckeditor('descricao', $dados[descricao]); ?>
        </div>




<?php /*
        <script>
                $(document).ready(function() {
                    var max_fields      = 10; //maximum input boxes allowed
                    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
                    var add_button      = $(".add_field_button"); //Add button ID

                    var x = 1; //initlal text box count
                    $(add_button).click(function(e){ //on add input button click
                        e.preventDefault();
                        if(x < max_fields){ //max input box allowed
                            x++; //text box increment
                            $(wrapper).append('<div class="clearfix"></div>  <div class="">  <div class="input_fields_wrap row col-xs-10"><div class="col-xs-12 form-group"><label>Descrição</label><input class="form-control fundo-form1 input100" type="text" name="aplicacao[]" value="" ></div>  </div>   <a href="#" class="remove_field btn btn-danger top25">Excluir</a> </div>    '); //add input box
                        }
                    });

                    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                        e.preventDefault(); $(this).parent('div').remove(); x--;
                    })
                });
            </script>


            <div class="col-xs-12">
                <label>Aplicações</label>
            </div>




             <?php
            //  verifico se exixtem itens cadastrados
             $sql = "select * from tb_produtos_aplicacoes where id_produto = '$_GET[id]'";
             $result = parent::executaSQL($sql);
             if (mysql_num_rows($result) == 0) {
             ?>
                <div class="input_fields_wrap row col-xs-12">
                    <div class="col-xs-12 form-group">
                        <label>Descrição</label>
                        <input class="form-control fundo-form1 input100" type="text" name="aplicacao[]" value="" >
                    </div>

                </div>
             <?php
             }else{
                while ($row = mysql_fetch_array($result)) {
                ?>

                    <div class="row col-xs-12">
                        <div class="col-xs-12 form-group">
                            <label>Descrição</label>
                            <input class="form-control fundo-form1 input100" type="text" name="aplicacao[]" value="<?php Util::imprime($row[titulo]) ?>" >
                        </div>
                    </div>

                <?php
                }

                echo '<div class="input_fields_wrap"></div>';
             }
            ?>








            <div class="col-xs-12 bottom30">
                <button class="add_field_button btn btn-primary">Adicionar mais campo</button>
            </div>








*/ ?>




        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->
        <?php require_once("../includes/google_seo.php"); ?>
        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->





        <!-- ======================================================================= -->
        <!-- VALIDACAO DO FORMULARIO    -->
        <!-- ======================================================================= -->
        <script>
          $(document).ready(function() {
            $('.FormPrincipal').bootstrapValidator({
              message: 'This value is not valid',
              feedbackIcons: {
								valid: 'fa fa-check',
								invalid: 'fa fa-remove',
								validating: 'fa fa-refresh'
              },
              fields: {


              titulo: {
                validators: {
                  notEmpty: {

                  }
                }
              },

              id_categoriaproduto: {
                validators: {
                  notEmpty: {

                  }
                }
              },


              marca: {
                validators: {
                  notEmpty: {

                  }
                }
              },

            id_subcategoriaproduto: {
                validators: {
                  notEmpty: {

                  }
                }
              },


              valor: {
                validators: {
                  notEmpty: {

                  },
                  numeric: {
                        transformer: function($field, validatorName, validator) {
                            var value = $field.val();
                            return value.replace(',', '');
                        }
                    }
                }
              },


              email: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                }
              },

              mensagem: {
                validators: {
                  notEmpty: {

                  }
                }
              }
            }
          });
          });
        </script>




            <?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");

                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 236, 212);
            }

            //	CADASTRA O USUARIO
            $id = parent::insert($this->nome_tabela, $dados);

            // CADASTRA APLICACOES
            $this->cadastra_aplicacoes($id, $dados);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


            Util::script_msg("Cadastro efetuado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");


                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 236, 212);
            }

            parent::update($this->nome_tabela, $id, $dados);

            // CADASTRA APLICACOES
            $this->cadastra_aplicacoes($id, $dados);



            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



            Util::script_msg("Alterado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}


    /* ==================================================================================================================  */
    /*  CADASTRA OS INTENS  */
    /*  ==================================================================================================================  */
    public function cadastra_aplicacoes($id, $dados)
    {

        // APAGO OS ITENS DO AGRUPAMENTO
        $sql = "delete from tb_produtos_aplicacoes where id_produto =  $id";
        parent::executaSQL($sql);

        //  CADASTO OS ITENS
        if(count($dados[aplicacao]) > 0){
            foreach ($dados[aplicacao] as $key => $titulo) {
                if ( !empty($titulo) ) {
                    $titulo = Util::trata_dados_formulario($titulo);
                    $sql = "
                            insert into tb_produtos_aplicacoes
                                (id_produto, titulo)
                            values
                                ('$id', '$titulo' )
                            ";
                    parent::executaSQL($sql);
                }

            }
        }


    }




	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
            if($ativo == "SIM")
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
            else
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
            //	BUSCA OS DADOS
            $row = $this->select($id);

            $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
            if($id != "")
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            WHERE
                                    " . $this->chave_tabela. " = '$id'
                            ";
                    return mysql_fetch_array(parent::executaSQL($sql));
            }
            else
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            ORDER BY
                                    ordem
                            ";
                    return parent::executaSQL($sql);
            }

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
            $obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
            $obj_imagem->redimensiona($largura, $altura, $tipo);
            $obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
