
<!--  ==============================================================  -->
<!-- UNIDADES-->
<!--  ==============================================================  -->
<div class="col-xs-6  unidades_contatos top20">
	<div class="media">
		<div class="media-left media-middle ">
			<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/unidades_contatos.jpg" alt="">
		</div>
		<div class="media-body">
			<h1 class="media-heading lato-bold">
				<?php echo Util::imprime($config[titulo]); ?>
			</h1>
		</div>
	</div>

</div>

<div class="col-xs-6 top10 ">
	<div class="top15 unidades_contatos">
		<div class="dropdown font-futura ">
			<button class="btn btn_outras_geral  col-xs-12 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				SELECIONAR OUTRA UNIDADE
				<span class="fa fa-angle-down left10"></span>
			</button>
			<ul class="dropdown-menu col-xs-offset-2 col-xs-10" aria-labelledby="dropdownMenu1">
				<?php
		          $result = $obj_site->select("tb_unidades","limit 5");
		          if (mysql_num_rows($result) > 0) {
		            $i = 0;
		            while($row = mysql_fetch_array($result)){
		              ?>
					<li><a class="btn btn_outras_unidades" href="?unidade=<?php echo $row[url_amigavel] ?>"><?php Util::imprime($row[titulo]); ?></a></li>
		              <?php
			         }
			    }
			    ?>
			</ul>
		</div>
	</div>
</div>
<!--  ==============================================================  -->
<!-- UNIDADES-->
<!--  ==============================================================  -->





	<!--  ==============================================================  -->
	<!-- CONTATOS-->
	<!--  ==============================================================  -->
	<div class="col-xs-12 padding0 contatos_topo padding0 top10">
		<div class="media pull-left">
			<div class="media-left media-middle ">
				<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone-telefone.png" alt="">
			</div>
			<div class="media-body">
				<h2 class="media-heading lato-bold">
					<span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
				</h2>

			</div>
		</div>


		<?php if (!empty($config[telefone2])): ?>

			<div class="media pull-left left15">
				<div class="media-left media-middle ">
					<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone-telefone.png" alt="">
				</div>
				<div class="media-body">
					<h2 class="media-heading lato-bold">
						<span><?php Util::imprime($config[ddd2]) ?></span><?php Util::imprime($config[telefone2]) ?>
					</h2>

				</div>
			</div>
		<?php endif; ?>

		<?php if (!empty($config[telefone3])): ?>

			<div class="media pull-left left15">
				<div class="media-left media-middle ">
					<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone-telefone.png" alt="">
				</div>
				<div class="media-body">
					<h2 class="media-heading lato-bold">
						<span><?php Util::imprime($config[ddd3]) ?></span><?php Util::imprime($config[telefone3]) ?>
					</h2>

				</div>
			</div>
		<?php endif; ?>

		<?php if (!empty($config[telefone4])): ?>

			<div class="media pull-left left15">
				<div class="media-left media-middle ">
					<img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone-telefone.png" alt="">
				</div>
				<div class="media-body">
					<h2 class="media-heading lato-bold">
						<span><?php Util::imprime($config[ddd4]) ?></span><?php Util::imprime($config[telefone4]) ?>
					</h2>

				</div>
			</div>
		<?php endif; ?>
	</div>
	<!--  ==============================================================  -->
	<!-- CONTATOS-->
	<!--  ==============================================================  -->

	<div class="clearfix">	</div>
