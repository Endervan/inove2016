<div class="clearfix"></div>
<div class="container-fluid rodape top90">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>
		<div class="container top40">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-xs-2 top10 bottom15">
					<a href="#">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->
				<div class="col-xs-10">
					<div class="barra_branca">
						<ul class="menu-rodape">
							<li>
								<a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">
									HOME
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/empresa">
									A EMPRESA
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "produtos-categoria" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos">
									PRODUTOS
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "galerias" or Url::getURL( 0 ) == "galeria"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/galerias">
									GALERIA
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "medidas"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/medidas">
									MEDIDAS
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/dicas"
									>DICAS
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
									FALE CONOSCO
								</a>
							</li>
							<li>
								<a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabale-conosco">
									TRABALHE CONOSCO
								</a>
							</li>

						</ul>
					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- MENU    -->
				<!-- ======================================================================= -->





				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="col-xs-7 top30 telefone_rodape">
					<p class="bottom15"><i class="fa fa-home right10"></i><?php Util::imprime($config[endereco]); ?></p>
					<p class="bottom15">
						<i class="fa fa-phone right10"></i>
						<?php Util::imprime($config[ddd1]); ?>
						<?php Util::imprime($config[telefone1]); ?>
						<?php if (!empty($config[telefone2])) { ?>
							/
							<?php Util::imprime($config[ddd2]); ?>
							<?php Util::imprime($config[telefone2]); ?>
						<?php } ?>
						<?php if (!empty($config[telefone3])) { ?>
							/
							<?php Util::imprime($config[ddd3]); ?>
							<?php Util::imprime($config[telefone3]); ?>
						<?php } ?>
						<?php if (!empty($config[telefone4])) { ?>
							<span class="left15"></span>
							<i class="fa fa-whatsapp" aria-hidden="true"></i>
							<?php Util::imprime($config[ddd4]); ?>
							<?php Util::imprime($config[telefone4]); ?>
						<?php } ?>


						</p>

					</div>
					<!-- ======================================================================= -->
					<!-- ENDERECO E TELEFONES    -->
					<!-- ======================================================================= -->


					<div class="col-xs-2 text-right top25">
						<?php if ($config[google_plus] != "") { ?>
							<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
								<i class="fa fa-google-plus right15"></i>
							</a>
							<?php } ?>

							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>






		<div class="container-fluid">
			<div class="row rodape-preto">
				<div class="col-xs-12 text-center top15 bottom15">
					<h5>© Copyright INOVE UNIFORMES</h5>
				</div>
			</div>
		</div>
