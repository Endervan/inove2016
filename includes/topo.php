

<div class="container-fluid topo">
  <div class="row">

    <div class="container">
      <div class="row">
        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->
        <div class="col-xs-2 text-center top5">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="HOME">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
          </a>
        </div>

        <!--  ==============================================================  -->
        <!-- LOGO -->
        <!--  ==============================================================  -->

        <div class="col-xs-10 padding0">
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->
          <div class="col-xs-8 padding0 contatos_topo top5">
            <div class="media pull-right">
              <div class="media-left media-middle icon-telefone ">
                <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icone-telefone.png" alt="">
              </div>
              <div class="media-body">
                <h2 class="media-heading lato-bold">
                  <span><?php Util::imprime($config[ddd1]) ?></span><?php Util::imprime($config[telefone1]) ?>
                </h2>

              </div>
            </div>


            <?php if (!empty($config[telefone4])): ?>
              <div class="media pull-right right15">
                <div class="media-left media-middle ">
                  <i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                  <h2 class="media-heading lato-bold">
                    <span><?php Util::imprime($config[ddd4]) ?></span><?php Util::imprime($config[telefone4]) ?>
                  </h2>

                </div>
              </div>
            <?php endif; ?>

          </div>
          <!--  ==============================================================  -->
          <!-- CONTATOS -->
          <!--  ==============================================================  -->

          <!--  ==============================================================  -->
          <!-- ESTOU EM -->
          <!--  ==============================================================  -->
          <div class="col-xs-4 top5 div_personalizada text-right">
            <!-- ======================================================================= -->
            <!-- UNIDADES  falta fazer ajax pra troca os numeros de acordo com unidade selecionada -->
            <!-- ======================================================================= -->
            <div class="unidades">
              <div class="col-xs-4">  <span>Estou em:</span></div>
              <select class="form-control text-center" id="appearance-select1" onchange="javascript:location.href = this.value;">

                <?php
                $result = $obj_site->select("tb_unidades","limit 5");
                if (mysql_num_rows($result) > 0) {
                  $i = 0;
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <option value="?unidade=<?php echo $row[url_amigavel] ?>" <?php if($config[idunidade] == $row[idunidade] ){ echo 'selected'; } ?> class="btn btn_outras_unidades" ><?php echo Util::imprime($row[titulo]); ?></option>
                    <?php

                  }
                }
                ?>
              </select>
            </div>
            <!-- ======================================================================= -->
            <!-- UNIDADES   -->
            <!-- ======================================================================= -->
          </div>
          <!--  ==============================================================  -->
          <!-- ESTOU EM -->
          <!--  ==============================================================  -->


          <!--  ==============================================================  -->
          <!-- CATEGORIAS programar com 7 itens -->
          <!--  ==============================================================  -->
          <div class="col-xs-12 menu_cat top5 bottom15">
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
              <ul class="nav navbar-nav navbar-right">


                <?php
                $result = $obj_site->select("tb_categorias_produtos", "order by rand() limit 7");
                if (mysql_num_rows($result) > 0) {
                  while($row = mysql_fetch_array($result)){
                    //  busca a qtd de produtos cadastrados
                    //$result1 = $obj_site->select("tb_produtos_modelos_aceitos", "and id_tipoveiculo = '$row[idtipoveiculo]'");


                    switch ($i) {
                      case 1:
                      $cor_class = 'ÁREA DE SAÚDE';
                      break;
                      case 2:
                      $cor_class = 'HOTELARIA';
                      break;
                      case 3:
                      $cor_class = 'BARES/RESTAURANTES';
                      break;
                      case 4:
                      $cor_class = 'LIMPEZA EM GERAL';
                      break;
                      case 5:
                      $cor_class = 'CONSTRUÇÃO CIVIL';
                      break;

                      default:
                      $cor_class = 'INDÚSTRIAS EM GERAIS';
                      break;
                    }
                    $i++;
                    ?>

                    <li>
                      <a href="<?php echo Util::caminho_projeto() ?>/produtos-categoria/<?php Util::imprime($row[url_amigavel]); ?>">
                        <span class="clearfix "><img width="30" height="26" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" /></span>
                        <?php Util::imprime($row[titulo]); ?>
                      </a>
                    </li>

                    <?php
                  }
                }
                ?>

                <li>

                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->
                  <div class="col-xs-12 dropdown padding0">
                    <a class="btn btn_carrinho input100" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <img src="<?php echo Util::caminho_projeto(); ?>/imgs/carrinhop_topo.png" alt="" />
                    </a>


                    <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">

                      <!--  ==============================================================  -->
                      <!--sessao adicionar produtos-->
                      <!--  ==============================================================  -->
                      <?php
                      if(count($_SESSION[solicitacoes_produtos]) > 0)
                      {
                        for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                        {
                          $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                          ?>
                          <div class="lista-itens-carrinho">
                            <div class="col-xs-2">
                              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                            </div>
                            <div class="col-xs-8">
                              <h1><?php Util::imprime($row[titulo]) ?></h1>
                            </div>
                            <div class="col-xs-1">
                              <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-remove"></i> </a>
                            </div>
                            <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>
                          </div>
                          <?php
                        }
                      }
                      ?>


                      <!--  ==============================================================  -->
                      <!--sessao adicionar servicos-->
                      <!--  ==============================================================  -->
                      <?php
                      if(count($_SESSION[solicitacoes_servicos]) > 0)
                      {
                        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                        {
                          $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                          ?>
                          <div class="lista-itens-carrinho">
                            <div class="col-xs-2">
                              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                            </div>
                            <div class="col-xs-8">
                              <h1><?php Util::imprime($row[titulo]) ?></h1>
                            </div>
                            <div class="col-xs-1">
                              <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                            </div>
                            <div class="col-xs-12"><div class="borda_carrinho top5">  </div> </div>

                          </div>
                          <?php
                        }
                      }
                      ?>




                      <div class="col-xs-12 text-right top10 bottom20">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_amarelo" >
                          ENVIAR ORÇAMENTO
                        </a>
                      </div>
                    </div>
                  </div>
                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->



                </li>

              </ul>

            </div><!--/.nav-collapse -->
          </div>
          <!--  ==============================================================  -->
          <!-- CATEGORIAS -->
          <!--  ==============================================================  -->




          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->
          <div class="col-xs-12 menu">
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
              <ul class="nav navbar-nav navbar-right font-futura">
                <li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
                </li>
                <li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto" or Url::getURL( 0 ) == "produtos-categoria" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "galerias" or Url::getURL( 0 ) == "galeria"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/galerias">GALERIA</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "medidas"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/medidas">MEDIDAS</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica" or Url::getURL(0)== "uniformes"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
                </li>

                <li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
                </li>

              </ul>

            </div><!--/.nav-collapse -->
          </div>
          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->

        </div>

      </div>
    </div>
  </div>
</div>

<?php /*


*/ ?>
