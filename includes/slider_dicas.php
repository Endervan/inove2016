<?php
$qtd_itens_linha = 4; // quantidade de itens por linha
$id_slider = "carousel-example-generic-equipe";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="carousel slide bottom70 slider-parceiros-clientes" data-ride="carousel" interval="99999999" pause="hover">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_dicas","order by rand() limit 12");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_dicas", "limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>
        <div class="item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>
              <div class="col-xs-6 padding0 text-center top30">
                <div class="lista-parceiros">


                  <div class="col-xs-4 div_personalizada">
                    <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($linha[$c_temp][url_amigavel]); ?>" title="<?php Util::imprime($linha[$c_temp][titulo]); ?>">

                      <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 100, 100, array("class"=>"img-circle top15 bottom15", "alt"=>"$linha[$c_temp][titulo]") ) ?>

                    </div>
                    <div>
                      <?php /*<h1><?php Util::imprime($linha[$c_temp][titulo]) ?></h1>*/ ?>
                    </div>
                    <div class="col-xs-8 dicas_descricao_home top20 padding0">
                      <p><?php Util::imprime($linha[$c_temp][descricao], 300) ?></p>
                    </div>
                  </a>
                </div>
              </div>
              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>



  <!-- Controls -->
  <a class="left carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <i class="fa fa-chevron-left" aria-hidden="true"></i>
  </a>
  <a class="right carousel-control" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <i class="fa fa-chevron-right" aria-hidden="true"></i>
  </a>
</div>
